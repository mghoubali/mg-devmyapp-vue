import Vue from 'vue'
import Vuetify from 'vuetify'
import App from './App'
import * as firebase from 'firebase'
import router from './router'
import 'vuetify/dist/vuetify.min.css'
import { store } from './store'
import DateFilter from './filters/dateFilters'
import AlertCmp from './components/Shared/Alert.vue'
import colors from 'vuetify/es5/util/colors'
import EditMeetupDetailsDialog from './components/Meetup/Edit/EditMeetupDetailsDialog.vue'
import EditMeetupDateDialog from './components/Meetup/Edit/EditMeetupDateDialog.vue'
import EditMeetupTimeDialog from './components/Meetup/Edit/EditMeetupTimeDialog.vue'
import RegisterDialog from './components/Meetup/Registration/RegistrationDialog.vue'

Vue.use(Vuetify, {
  theme: {
    primary: colors.blue.accent4,
    secondary: colors.blue.accent1,
    accent: colors.purple.base,
    error: colors.red.base,
    warning: colors.orange.base,
    info: colors.lightBlue.base,
    success: colors.green.base
  }
})

Vue.config.productionTip = false

Vue.filter('dateFilter', DateFilter)
Vue.component('app-alert', AlertCmp)
Vue.component('app-edit-meetup-details-dialog', EditMeetupDetailsDialog)
Vue.component('app-edit-meetup-date-dialog', EditMeetupDateDialog)
Vue.component('app-edit-meetup-time-dialog', EditMeetupTimeDialog)
Vue.component('app-meetup-registration-dialog', RegisterDialog)

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  render: h => h(App),
  created () {
    firebase.initializeApp({
      apiKey: 'AIzaSyABaLPzaEvRg9p6jXSSwvEHvlZweWEX57I',
      authDomain: 'meetup-app-mg.firebaseapp.com',
      databaseURL: 'https://meetup-app-mg.firebaseio.com',
      projectId: 'meetup-app-mg',
      storageBucket: 'meetup-app-mg.appspot.com',
      messagingSenderId: '1012271045509'
    })
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        this.$store.dispatch('autoSignIn', user)
        this.$store.dispatch('fetchUserData')
      }
    })
    this.$store.dispatch('loadMeetupsFromFirebase')
  }
})
